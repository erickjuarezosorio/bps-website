

$(document).ready(function () {
  $( ".langes" ).removeClass("active");
  $( ".langen" ).removeClass("active");
  $( ".selectitem" ).removeClass("active");

if ($(".submenu .menu-item" ).hasClass("active")) {
  $( ".selectitem .nav-link span" ).text($(".submenu .menu-item.active .nav-link span").text());
}

$( ".play-btn" ).click(function(e) {
  e.preventDefault();
  $('.modal').modal('hide')
  $('#videoModal').modal('toggle')
});
$('#videoModal').on('hidden.bs.modal', function () {
  $("#videoModal iframe").attr("src", $("#videoModal iframe").attr("src"));
});
$( ".selectitem" ).click(function(e) {
  e.preventDefault();
  $('#searchModal').modal('hide')
  $('#menuModal').modal('toggle')
});
$( ".search a" ).click(function(e) {
  e.preventDefault();
  $('#menuModal').modal('hide')
  $('#searchModal').modal('toggle')
});
$( ".navbar-expand-lg .btn" ).click(function(e) {
  e.preventDefault();
  $('.modal').modal('hide')
  $('#mobileModal').modal('toggle')
});
$( ".close-modal" ).click(function(e) {
  e.preventDefault();
  $('.modal').modal('hide')
});



function menuSticky() {
  var posSection = $("main");

  $(document).scroll(function () {
    posSection.each(function () {
      var posRow = $(this).offset().top;
      var scrolleo = $(window).scrollTop();

      if (scrolleo > posRow) {
        $(".header").addClass("activo");
      } else {
        $(".header").removeClass("activo");
      }
    });
  });
}

menuSticky();

function validacionSesion(){
  $('.home .botonera a').on("click", function (e) {
    // CHECKBOX
    if($('#checkEdad').prop("checked") == false){
      $('.msjError').remove();
      $('#cajaEdad').append('<span class="msjError" > Debes ser mayor de 18 años. </span>');
      e.preventDefault();
    }

    else if($('#checkLegale').prop("checked") == false){
      $('.msjError').remove();
      $('#cajaLegal').append('<span class="msjError" > Debes aceptar los términos y condiciones. </span>');
      e.preventDefault();
    }

    else {
      $('.msjError').remove();
      $(".registro .form img").addClass("imgOn");
    }

    // VALIDATE ALL

    if ($('.ticket .error-input').length == 0) {
      //    window.location.href = "/completar-registro"
    }

  });
}

validacionSesion();


/// ANCLAS

function anclas(){
  $(".home--pleca h4").on("click", function () {
    $("html, body").animate({ scrollTop: $(".sesion").offset().top - 200 }, 1500);
  });
}

anclas();


// CARRUSELES

function carrusel(){
  var mySwiper = new Swiper("#slider", {
    autoplay:false,
    direction: 'horizontal',
    loop: true,
    slidesPerView: 1.8,
    spaceBetween: 25,
    centeredSlides: false,
    pagination: {
      el: '.swiper-pagination',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
  var mySwiper2 = new Swiper(".slider-servicios", {
    direction: 'horizontal',
    loop: false,
    slidesPerView: 1.3,
    spaceBetween: 16,
    centeredSlides: false,
    pagination: {
      el: '.swiper-pagination',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      480: {
        slidesPerView: 1.8,
      }
    }
  });
  var mySwiper3 = new Swiper("#solucionesuno", {
    autoplay:false,
    direction: 'horizontal',
    loop: false,
    slidesPerView: 1.1,
    spaceBetween: 12,
    centeredSlides: false,
    pagination: {
      el: '.swiper-pagination',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      480: {
        slidesPerView: 2.4,
        spaceBetween: 25,
      }
    }
  });
  var mySwiper4 = new Swiper(".slider-vertical", {
    autoplay: false,
    direction: 'vertical',
    loop: true,
    slidesPerView: 1,
    spaceBetween: 0,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
    },
  });


  var mySwiper5 = new Swiper(".slider-clientes", {
    autoplay: false,
    direction: 'horizontal',
    loop: true,
    slidesPerView: 1,
    spaceBetween: 25,
    centeredSlides: false,

    navigation: {
      nextEl: '.swiper-button-next',
    },
    breakpoints: {
      480: {
        slidesPerView: 3,
      }
    }
  });
  var mySwiper6 = new Swiper(".slider-vertical2", {
    autoplay: false,
    direction: 'vertical',
    centeredSlides: true,
    loop: true,
    slidesPerView: 5,
    spaceBetween: 0,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
    },
  });
  var mySwiper7 = new Swiper(".slider-centered", {
    direction: "horizontal",
    slidesPerView: 1.5,
    centeredSlides: true,
    spaceBetween: -150,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    var mySwiper8 = new Swiper(".slider-certificaciones", {
      direction: "horizontal",
      slidesPerView: 2,
      centeredSlides: false,
      spaceBetween: 30,
      loop: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
}
  var mySwiper9 = new Swiper(".slider-vertical3", {
    autoplay: false,
    direction: 'vertical',
    loop: true,
    slidesPerView: 2.1,
    spaceBetween: 0,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
    },
    breakpoints: {
      480: {
        slidesPerView: 1.6,
      }
    }
  });
  var mySwiper10 = new Swiper(".slider-standard", {
    autoplay: false,
    loop: true,
    slidesPerView: 1,
    spaceBetween: 0,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
    },
  });
}

carrusel()

/// FORMULARIOS

function inputs(){
  $(".form__input input, .form__input select").focus(function () {
    $(this).parents(".form__input").addClass("on");
  });

  $(".form__input input, .form__input select").blur(function () {

    if($(this).val()===""){
      $(this).parents(".form__input").removeClass("on");
    } else{
      $(this).parents(".form__input").addClass("on");
    }

  });
}

inputs();

function uploadFile(){
  $("#img_ticket").on("change",function(e) {
    var filename = e.target.files[0].name;
    $("#btnFileFalse span").text( filename);
  });
}

uploadFile();

////////// REGEX FORMS ///////////

/// TEXTO

function valText(nameInput) {

  var regEx = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/;
  var validText = regEx.test(nameInput.val());

  if (nameInput.val().length < 1) {
    nameInput.siblings("span").remove();
    nameInput.after("<span class='msjError'>Campo requerido");
  } else {
    nameInput.siblings("span").remove();

    if (!validText) {
      nameInput.siblings("span").remove();
      nameInput.after('<span class="msjError"> Solo se aceptan letras </span>');
      nameInput.siblings("span").show();
    } else {
      nameInput.siblings("span").remove();
    }
  }


}

/// NUMEROS

function valNum(nameInput) {
  if (nameInput.val().length < 1) {
    nameInput.siblings("span").remove();
    nameInput.after("<span class='msjError'>Campo requerido");
  } else {

    var regEx = /^[0-9]+$/;

    var validEmail = regEx.test(nameInput.val());

    if (!validEmail) {
      nameInput.siblings("span").remove();
      nameInput.after('<span class="msjError"> Solo se permiten numeros </span>');
      nameInput.siblings("span").show();
    } else {
      nameInput.siblings("span").remove();
    }
  }
}

/// COMBOS

function valCombos(nameInput){
  var combosContacto = $(nameInput);

  combosContacto.each(function () {
    $(this).change(function () {
      if ($(this).val() != null) {
        $(this).siblings("span").remove();

      }
    });

    if ($(this).val() == "") {
      $(this).siblings("span").remove();
      $(this).after('<span class="msjError"> Campo requerido </span>');
    }

  });

}

// COMPLETAR REGISTRO

function completarRegistro(){
  $('#enviarRegistro').on("click", function() {

    var nombre = $("#nombre"),
    apellido = $("#apellido"),
    telefono = $("#telefono"),
    estado = $("#selecEdo");

    valText(nombre);
    valText(apellido);
    valNum(telefono);
    valCombos(estado);

    // VALIDATE ALL

    if ($('#formCompletarRegistro .msjError').length == 0) {
      $("#formCompletarRegistro").submit();
      window.location.href="/gracias-registro/";
    }

    return false;
  });
}

completarRegistro();

/// HEADER

function header(){
  $(".header--navegacion .btn").on("click", function () {
    $(".header").toggleClass("headerOn");
    $(this).toggleClass("nav-on");
  });

  $("#header--logo").on("click", function () {
    history.replaceState(null, null, ' ');

    $("html, body").animate({ scrollTop: 0 }, 1500);
    $(".header--navegacion .btn").removeClass("nav-on");
    $(".navbar-collapse").removeClass("show");
    $(".header").removeClass("headerOn");

  });

  $("#navbarNav li").on("click", function () {
    var nameLi = $(this).attr("id").replace("li-", ""),
    nameSection = $("." + nameLi).offset().top;

    $("html, body").animate({ scrollTop: nameSection = $("." + nameLi).offset().top - 90 }, 1500);

    $(".header--navegacion .btn").removeClass("nav-on");
    $(".navbar-collapse").removeClass("show");

    $("#navbarNav li").removeClass("activo");
    $(this).addClass("activo");

    $(".header").removeClass("headerOn");

    return false;

  });

  var posSection = $("section");

  $(document).scroll(function () {
    posSection.each(function () {
      var posRow = $(this).offset().top - 150;
      var scrolleo = $(window).scrollTop();
      var nameLi = $(this).attr("class").replace("s", "li-s");

      if (scrolleo > posRow) {
        $("#navbarNav li").removeClass("activo");
        $("#"+nameLi).addClass("activo");
      }

    });

  });
}

/// EFECTOS

function efectoFade() {

  function sectionFade() {
    var sectionFade = $(".sectionFade");
    sectionFade.each(function () {
      var posSection = $(this).offset().top - 450;
      var scrolleo = $(window).scrollTop();

      if (scrolleo > posSection) {
        $(this).find($(".objetFade")).addClass("transition1");
      } else {
        $(this).find($(".objetFade")).removeClass("transition1");
      }

    });
  }

  sectionFade();

  $(document).scroll(function() {
    sectionFade();
  });
}


// FORMULARIO

function formularios(){
  $(".checkboxes").on("click", function () {
    if ( $(this).find("input").attr("checked")) {
      $(this).find("input").removeAttr("checked");
    }

    else {
      $(this).find("input").attr("checked","checked");
    }

  });

  $("#selectEdo").on("change", function () {
    $("#linkBoton").attr({ "href": $(this).val(), "target": "_blank" });
    $("#errorCombo").remove();
  });

  $("#boton-buscar").on("click", function () {

    if ($("#selectEdo").val() == null) {
      $("#errorCombo").remove();
      $(".combo").after("<span class='error-input' id='errorCombo'>Elige un estado");
    }

    else {
      $("#errorCombo").remove();

      if ($(".checkbox[checked]").length == 2) {
        $("#errorLegales").remove();
        var data = "Especialista-" + jQuery("#selectEdo option:selected").text();

        dataLayer.push({
          'event': data
        });

        return true;
      }

      else{
        $("#errorLegales").remove();
        $("#checkTerminos").after("<span class='error-input' id='errorLegales'>Debes aceptar el Aviso de Privacidad y Términos y Condiciones");

        return false;
      }
    }

  });
}


/// COMBO DATA LAYER

// (function() {
//     var selectMenu = document.querySelector('#selectEdo');
//     var callback = function(e) {
//     var selectedOption = selectMenu.options[selectMenu.selectedIndex].id;

//       window.dataLayer.push({
//         event: selectedOption,
//         // selectedElement: selectedOption
//       });

//     };

//     selectMenu.addEventListener('change', callback, true);

// })();


// ACORDEON PLUS
$('.acordeon').on('click',function(){
  $(this).toggleClass('active');
  // $(this).removeClass('active');
});

});
